import _ from "lodash";
import { ApolloServer } from "@apollo/server";
import fastifyApollo, {
  fastifyApolloDrainPlugin,
} from "@as-integrations/fastify";

import app from "./app";
import userTypeDefs from "./graphql/typeDefs/userTypeDef";
import userResolver from "./graphql/resolvers/userResolver";
import { authMiddleware, isAdmin } from "./middlewares/authMiddleware";
import MyContext from "./types/interfaces/MyContext";
import { FastifyReply, FastifyRequest } from "fastify";

const myPort = Number(process.env.PORT) || 5050;

const baseTypeDefs = `
  type Query
  type Mutation
`;

const apollo = new ApolloServer<MyContext>({
  typeDefs: [baseTypeDefs, userTypeDefs],
  resolvers: _.merge({}, userResolver),
  plugins: [fastifyApolloDrainPlugin(app)],
});

apollo.start().then(() => {
  app.register(fastifyApollo(apollo), {
    context: async (request: FastifyRequest, reply: FastifyReply) => {
      return {
        isAuthenticated: authMiddleware(request),
        isAdmin: isAdmin(request),
        reply,
        request,
      } as any;
    },
  });
  app.listen({ port: myPort }, async (err: Error | null, address: string) => {
    if (err) {
      console.error(err);
      process.exit(1);
    }
    console.log(`Server is listening at ${address}`);
  });
});

export { app };
export default apollo;
