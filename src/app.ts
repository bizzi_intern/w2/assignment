import fastify, { FastifyReply, FastifyRequest } from "fastify";
import type { FastifyCookieOptions } from "@fastify/cookie";
import cookie from "@fastify/cookie";
import cors from "@fastify/cors";
import dotenv from "dotenv";

dotenv.config({ path: `.env.${process.env.NODE_ENV}` });

declare module "fastify" {
  interface FastifyContext {
    isAuthenticated: boolean;
    isAdmin: boolean;
    request: FastifyRequest;
    reply: FastifyReply;
  }
}

// App config
const app = fastify({ logger: true });

// Error Handler
const errorHandler = (
  error: Error,
  request: FastifyRequest,
  reply: FastifyReply
) => {
  const statusCode = reply.statusCode == 200 ? 500 : reply.statusCode;
  reply.status(statusCode);
  reply.send(error);
};

app.setErrorHandler(errorHandler);

app.register(cookie, {
  secret: process.env.COOKIE_SECRET,
  parseOptions: {},
} as FastifyCookieOptions);

app.register(cors, {
  origin: "*",
  credentials: true,
});

app.get("/", (request: FastifyRequest, reply: FastifyReply) => {
  reply.send({ hello: "world" });
});

app.get("/temp", (request: FastifyRequest, reply: FastifyReply) => {
  reply.send({ hello: "temp" });
});

export default app;
