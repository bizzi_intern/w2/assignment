const userTypeDefs = `#graphql
    type User {
        id: Int!
        email: String!
        password: String
        fullname: String!
        role: String!
        refresh_token: String
        created_at: String!
    }
    type UserWithoutId {
        email: String!
        password: String!
        fullname: String!
        role: String!
        refresh_token: String
    }
    type UserWithToken {
        id: Int!
        email: String!
        fullname: String!
        role: String!
        refresh_token: String
        created_at: String!
        access_token: String!
    }
    type UserQuery {
        id: Int!
        email: String!
        password: String
        fullname: String
        role: String
        refresh_token: String
        created_at: String
    }
    type accessToken {
        access_token: String!
    }
    type logoutMsg {
        logout: Boolean!
    }
    extend type Query {
        userByPk(id: Int!): UserQuery!
        userByEmail(email: String!): UserQuery!
    }
    extend type Mutation {
        register(fullname: String!, email: String!, password: String!): User!
        login(email: String!, password: String!): UserWithToken!
        handleRefreshToken: accessToken!
        logout: logoutMsg!
    }
`;

export default userTypeDefs;
