import bcrypt from "bcrypt";
import { FastifyContext } from "fastify";
import jwt from "jsonwebtoken";
import axios from "axios";
import User, {
  UserLogin,
  UserRegister,
} from "../../types/interfaces/UserInterface";
import genAccessToken from "../../config/genAccessToken";
import app from "../../app";
import genRefreshToken from "../../config/genRefreshToken";
import {
  INVALID_REFRESH_TOKEN_ERROR,
  NOT_AUTHENTICATED_ERROR,
  NO_REFRESH_TOKEN_ERROR,
  REFRESH_TOKEN_NOT_MATCHED_ERROR,
  UNAUTHORIZED_ACCESS_ERROR,
  UPDATE_REFRESH_TOKEN_FAIL_ERROR,
  USER_ALREADY_EXISTS_ERROR,
  VERIFY_TOKEN_ERROR,
  WRONG_PASSWORD_ERROR,
  WRONG_USERNAME_ERROR,
} from "../../lib/constants";

const saltRounds = 10;
const headers = {
  "x-hasura-admin-secret": process.env.X_HASURA_ADMIN_SECRET,
};

const userResolver = {
  Query: {
    userByPk: async (
      parent: any,
      args: { id: string },
      context: FastifyContext,
      info: any
    ) => {
      const { id } = args;
      let query;
      if (!context.isAuthenticated) {
        throw new Error(NOT_AUTHENTICATED_ERROR);
      }
      if (!context.isAdmin) {
        query = `#graphql
          query QueryUser {
            users_by_pk(id: "${id}") {
              id
              email
            }
          }
        `;
      } else {
        query = `#graphql
          query QueryUser {
            users_by_pk(id: "${id}") {
              id
              email
              password
              fullname
              role
              refresh_token
              created_at
            }
          }
        `;
      }

      const resFindUser = await axios.post(
        String(process.env.BASE_URI),
        { query },
        { headers }
      );
      console.log(resFindUser?.data);
      return resFindUser?.data?.data?.users_by_pk;
    },
    userByEmail: async (
      parent: any,
      args: { email: string },
      context: FastifyContext,
      info: any
    ) => {
      const { email } = args;
      let query;
      if (!context.isAuthenticated) {
        throw new Error(NOT_AUTHENTICATED_ERROR);
      }
      if (!context.isAdmin) {
        query = `#graphql
          query QueryUser {
            users_aggregate(where: {email: {_eq: "${email}"}}) {
              nodes {
                id
                email
              }
            }
          }
        `;
      } else {
        query = `#graphql
          query QueryUser {
            users_aggregate(where: {email: {_eq: "${email}"}}) {
              nodes {
                id
                email
                password
                fullname
                role
                refresh_token
                created_at
              }
            }
          }
        `;
      }

      const resFindUser = await axios.post(
        String(process.env.BASE_URI),
        { query },
        { headers }
      );
      return resFindUser?.data?.data?.users_aggregate?.nodes[0];
    },
  },
  Mutation: {
    register: async (
      parent: any,
      args: UserRegister,
      context: FastifyContext,
      info: any
    ) => {
      const { email, password, fullname } = args;
      const query = `#graphql
        query QueryUser {
          users_aggregate(where: {email: {_eq: "${email}"}}) {
            nodes {
              id
              email
              password
              fullname
              role
              refresh_token
              created_at
            }
          }
        }
      `;
      const resFindUser = await axios.post(
        String(process.env.BASE_URI),
        { query },
        { headers }
      );
      if (resFindUser?.data?.data?.users_aggregate?.nodes[0]) {
        throw new Error(USER_ALREADY_EXISTS_ERROR);
      }

      const salt = bcrypt.genSaltSync(saltRounds);
      const hashedPw = await bcrypt.hash(password, salt);

      // Create a new User
      const mutation = `#graphql
        mutation RegisterUser {
          insert_users_one(object: {email: "${email}", fullname: "${fullname}", password: "${hashedPw}"}) {
            id
            email
            fullname
            role
            refresh_token
            created_at
          }
        }
      `;

      const resRegisterUser = await axios.post(
        String(process.env.BASE_URI),
        { query: mutation },
        { headers }
      );

      return resRegisterUser?.data?.data?.insert_users_one;
    },
    login: async (
      parent: any,
      args: UserLogin,
      context: FastifyContext,
      info: any
    ) => {
      const { email, password } = args;
      const query = `#graphql
        query QueryUser {
          users_aggregate(where: {email: {_eq: "${email}"}}) {
            nodes {
              id
              email
              password
              fullname
              role
              refresh_token
              created_at
            }
          }
        }
      `;
      const resFindUser = await axios.post(
        String(process.env.BASE_URI),
        { query },
        { headers }
      );
      const findUser = resFindUser?.data?.data?.users_aggregate?.nodes[0];

      // Check if user exists
      if (!findUser) {
        throw new Error(WRONG_USERNAME_ERROR);
      }
      // Compare password
      if (!(await bcrypt.compare(password, findUser?.password))) {
        throw new Error(WRONG_PASSWORD_ERROR);
      }
      const refreshToken = genRefreshToken({
        id: findUser?.id,
        email: findUser?.email,
        role: findUser?.role,
      });

      // Update refresh token
      const { id } = findUser;
      const mutation = `#graphql
        mutation updateRefreshToken {
          update_users_by_pk(pk_columns: {id: "${id}"}, _set: {refresh_token: "${refreshToken}"}) {
            id
            email
            fullname
            created_at
            refresh_token
            role
          }
        }
      `;
      const resUpdateRefreshToken = await axios.post(
        String(process.env.BASE_URI),
        { query: mutation },
        { headers }
      );
      const updateUser = resUpdateRefreshToken?.data?.data?.update_users_by_pk;

      if (!updateUser) {
        throw new Error(UPDATE_REFRESH_TOKEN_FAIL_ERROR);
      }

      context.reply.cookie("refreshToken", refreshToken, {
        httpOnly: true,
        maxAge: 72 * 60 * 60 * 1000,
        sameSite: "none",
        secure: true,
      });

      return {
        ...updateUser,
        access_token: genAccessToken({
          id: updateUser?.id,
          email: updateUser?.email,
          role: updateUser?.role,
        }),
      };
    },
    handleRefreshToken: async (
      parent: any,
      args: null,
      context: FastifyContext,
      info: any
    ) => {
      const cookies = context.request.cookies;
      if (!cookies?.refreshToken) {
        throw new Error(NO_REFRESH_TOKEN_ERROR);
      }
      const refreshToken = cookies.refreshToken;

      const query = `#graphql
        query QueryUser {
          users_aggregate(where: {refresh_token: {_eq: "${refreshToken}"}}) {
            nodes {
              id
              email
              password
              fullname
              role
              refresh_token
              created_at
            }
          }
        }
      `;
      const resFindUser = await axios.post(
        String(process.env.BASE_URI),
        { query },
        { headers }
      );
      const user = resFindUser?.data?.data?.users_aggregate?.nodes[0];
      if (!user) {
        throw new Error(INVALID_REFRESH_TOKEN_ERROR);
      }

      return jwt.verify(
        refreshToken,
        process.env.JWT_SECRET as string,
        (err, decoded: any) => {
          if (err) {
            throw new Error(VERIFY_TOKEN_ERROR);
          }
          if (user?.id !== decoded?.id) {
            throw new Error(REFRESH_TOKEN_NOT_MATCHED_ERROR);
          }
          const access_token = genAccessToken({
            id: user?.id,
            email: user?.email,
            role: user?.role,
          });
          return { access_token };
        }
      );
    },
    logout: async (
      parent: any,
      args: null,
      context: FastifyContext,
      info: any
    ) => {
      if (!context.isAuthenticated) {
        throw new Error(NOT_AUTHENTICATED_ERROR);
      }
      const cookies = context.request.cookies;
      if (!cookies?.refreshToken) {
        throw new Error(NO_REFRESH_TOKEN_ERROR);
      }
      const refreshToken = cookies.refreshToken;

      const query = `#graphql
        query QueryUser {
          users_aggregate(where: {refresh_token: {_eq: "${refreshToken}"}}) {
            nodes {
              id
              email
              password
              fullname
              role
              refresh_token
              created_at
            }
          }
        }
      `;
      const resFindUser = await axios.post(
        String(process.env.BASE_URI),
        { query },
        { headers }
      );
      const user = resFindUser?.data?.data?.users_aggregate?.nodes[0];

      // No user found in db
      if (!user) {
        return { logout: false };
      }

      const { id } = user;
      const mutation = `#graphql
        mutation updateRefreshToken {
          update_users_by_pk(pk_columns: {id: "${id}"}, _set: {refresh_token: ""}) {
            id
            fullname
            email
            created_at
            refresh_token
            role
          }
        }
      `;
      const resUpdateRefreshToken = await axios.post(
        String(process.env.BASE_URI),
        { query: mutation },
        { headers }
      );
      context.reply.clearCookie("refreshToken", {
        httpOnly: true,
        secure: true,
      });

      return { logout: true };
    },
  },
};

export default userResolver;
