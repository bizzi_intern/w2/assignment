export default interface User {
  id: number;
  email: string;
  password: string;
  fullname: string;
  role: string;
  refresh_token?: string;
  created_at?: Date;
}

export interface UserToken {
  id: number;
  email: string;
  role: string;
}

export interface UserRegister {
  email: string;
  password: string;
  fullname: string;
}

export interface UserLogin {
  email: string;
  password: string;
}
