import { BaseContext } from "@apollo/server";
import { FastifyReply, FastifyRequest } from "fastify";

interface MyContext extends BaseContext {
  isAuthenticated?: boolean;
  isAdmin?: boolean;
  reply: FastifyReply;
  request: FastifyRequest;
}

export default MyContext;
