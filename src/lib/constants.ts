export const USER_ALREADY_EXISTS_ERROR = "User Already Exists Error";
export const WRONG_USERNAME_ERROR =
  "Wrong Username / User Not Registered Error";
export const WRONG_PASSWORD_ERROR = "Wrong Password Error";
export const NOT_AUTHENTICATED_ERROR = "Not Authenticated Error";
export const UNAUTHORIZED_ACCESS_ERROR = "Unauthorized Access Error";
export const NO_REFRESH_TOKEN_ERROR = "No Refresh Token In Cookies Error";
export const INVALID_REFRESH_TOKEN_ERROR =
  "Refresh Token Are Not In Database Or Does Not Matched Error";
export const REFRESH_TOKEN_NOT_MATCHED_ERROR =
  "Refresh Token Not Matched Error";
export const VERIFY_TOKEN_ERROR = "Fail To Verify Refresh Token Error";
export const NO_USER_IN_DB_ERROR = "No User In Database Error";
export const UPDATE_REFRESH_TOKEN_FAIL_ERROR =
  "Update Refresh Token Fail Error";
