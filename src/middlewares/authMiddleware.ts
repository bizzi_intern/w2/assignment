import jwt from "jsonwebtoken";

const authMiddleware = (req: any) => {
  let token: string;
  let isAuthenticated = false;
  if (req?.headers?.authorization) {
    token = req.headers.authorization;
    if (token) {
      const decoded: any = jwt.verify(token, String(process.env.JWT_SECRET));
      const user = {
        id: decoded.id,
        email: decoded.email,
        role: decoded.role,
      };
      req.user = user;
      isAuthenticated = true;
    }
  }
  return isAuthenticated;
};

const isAdmin = (req: any) => {
  if (req?.user?.role !== "admin") {
    return false;
  }
  return true;
};

export { authMiddleware, isAdmin };
