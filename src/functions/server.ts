import awsLambdaFastify from "@fastify/aws-lambda";
import { app } from "../index";

const proxy = awsLambdaFastify(app as any);

export const handler = async (event: any, context: any) => {
  const res = await proxy(event, context);
  return res;
};
