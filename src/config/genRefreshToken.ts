import jwt from "jsonwebtoken";
import { UserToken } from "../types/interfaces/UserInterface";

const genRefreshToken = (user: UserToken) => {
  return jwt.sign(user, String(process.env.JWT_SECRET), {
    expiresIn: "7d",
  });
};

export default genRefreshToken;
