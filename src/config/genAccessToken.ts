import jwt from "jsonwebtoken";
import { UserToken } from "../types/interfaces/UserInterface";

const genAccessToken = (user: UserToken) => {
  return jwt.sign(user, String(process.env.JWT_SECRET), {
    expiresIn: "1d",
  });
};

export default genAccessToken;
